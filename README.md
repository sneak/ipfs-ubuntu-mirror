# ipfs-ubuntu-mirror

based on sync_mirror.sh from JáquerEspeis

`docker run -d sneak/ipfs-ubuntu-mirror`

or

(image uses /var/lib/ubuntumirror and /var/lib/ipfs)

```
docker run \
    -v /host/dir/path/to/mirror:/var/lib/ubuntumirror \
    -v /host/dir/path/to/ipfsrepo:/var/lib/ipfs \
    -d sneak/ipfs-ubuntu-mirror
```

# requires

requires apt-transport-ipfs to use:

https://github.com/JaquerEspeis/apt-transport-ipfs

# see also

best-effort usually-up non-ha ipns pointer to ubuntu mirror:

`ipfs:/ipns/Qme4tKNduvAgQKN6nKjyH7KjyMdJwyPHfeVMp2EUS6b3J1`

viz:

```
NS=Qme4tKNduvAgQKN6nKjyH7KjyMdJwyPHfeVMp2EUS6b3J1
echo "deb ipfs:/ipns/$NS $(lsb_release -cs) main restricted universe multiverse" > /etc/apt/sources.list
```

(presently only supports xenial and bionic amd64)

# maintainer

Jeffrey Paul <sneak@sneak.berlin>
`5539AD00DE4C42F3AFE11575052443F4DF2A55C2`
